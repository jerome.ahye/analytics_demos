# example from 'R for marketing research and analytics'
#
# some common uses of linear models:
#
# - 'X' drivers analysis. In this case we are using survey data captured around
# satisfaction with an experience at a amusement park - so we would call it
# 'satisfaction drivers analysis'
#
# - Marketing mix modelling - Used to understand how price and advertising are
# related to £ metrics like sales
#
# + many other situations where want to model an outcome variable in terms of
# predictor variables. Once relationship is estimated, can use the model to make
# predictions or forecasts of likely outcome for other values of predictors.
#
# NOTE: The word 'driver' should not imply causation. A linear model only assumes
# an association among variables. e.g. price might be postively associated with
# satisfaction - but is the way to increase satsifaction to raise prices?
# - More likely price is associated with higher quality, which then leads to
# higher satisfaction.
# -- Results should be interpreted cautiously and considered in context of
# domain knowledge.
# ++ Links with McElreath / Judea Pearl and usage of carefully thought through
# DAGs when working through causal relationships.


# Amusement park data -----------------------------------------------------

# sat.df <- read.csv("http://goo.gl/HKn174")
# str(sat.df)

set.seed(08226)
nresp <- 500
# hypothetical survey with 4 Qs about a customer's satisfaction with different
# dimensions of visits to the amusement park.
# - satisfaction with rides, games, waiting times and cleanliness
# - with rating for overall satisfaction
# NOTE: In such surveys respondents often answer similarly on all satisfaction
# questions - known as the halo effect

# simulate a satisfaction halo with a random variable for each customer, that
# does not appear in the final data but is used to influence other ratings
halo <- rnorm(n = nresp, mean = 0, sd = 5)

# generate responses for satisfaction ratings by adding each respondents halo
# to the value or another random variable specific to the survey item.
# + include arbitrary constant to adjust range slightly
# + convert continuous values to integers using floor
# = a final value for each sat item on a 100 point scale
# NOTE: Although smaller likert scales are more common (1-5, 1-7 etc.) these
# scales introduce complications (discussed in chapt 7.9)


library(tidyverse)

sat_tibble <- tibble(rides = floor(halo + rnorm(n = nresp, mean = 80, sd = 3) + 1)) %>%
    dplyr::mutate(
        games =  floor(halo + rnorm(n = nresp, mean = 70, sd = 7) + 5),
        wait =  floor(halo + rnorm(n = nresp, mean = 65, sd = 10) + 9),
        clean =  floor(halo + rnorm(n = nresp, mean = 85, sd = 2) + 1)
        # by adding halo we create positive correlation between responses
    ) %>%
    # other Qs to add context to the customer's experience
    # sampled using log normal dist for distance and sampled discrete distributions
    # for weekend and number of children
    dplyr::mutate(
        distance = rlnorm(n = nresp, meanlog = 3, sdlog = 1),
        num_child = sample(x = 0:5, size = nresp, replace = TRUE,
                           prob = c(0.3, 0.15, 0.25, 0.15, 0.1, 0.05)),
        weekend = as.factor(sample(x = c("yes", "no"), size = nresp, replace = TRUE,
                                   prob = c(0.5, 0.5)))
    ) %>%
    # we create the overall satisfaction rating as a function of ratings for
    # the various aspects of the visit.
    dplyr::mutate(
        overall = floor(halo + 0.5*rides + 0.1*games + 0.3*wait + 0.2*clean
                         + 0.03*distance + 5*(num_child==0) + 0.3*wait*(num_child>0)
                         + rnorm(n = nresp, mean = 0, sd = 7) - 51)
    )

# NOTE: Although overall is a lengthy formula it can be understood in 5 parts
# 1. halo to capture latent satisfaction
# 2. adds weighted satisfaction variables
# 3. includes weighted contributions for other influences (distance + num_child)
# 4. random normal variation using rnorm()
# 5. floor to produce an integer with constant '-51' that adjust to be =< 100 points

# ** When a variable like overall is a linear combination of other variables
# plus random noise - we say it follows a linear model

# With real data one would wish to discover the contributions of the various
# elements, which are weights associated with various predictors

# Exploring the data --------------------------------------------------

# always begin with inspecting the data
summary(sat_tibble)

# gpairs function produces a scatterplot matrix visual that handles discrete
# and continuous variables better
install.packages("gpairs")

gpairs::gpairs(as.data.frame(sat_tibble))
# histograms all have normal distributions, except distance which is highly skewed
sat_tibble <- sat_tibble %>%
    dplyr::mutate(dist_log = log(distance))
gpairs::gpairs(as.data.frame(sat_tibble))

# Given the positive associations, we investigate the correlation structure
# further using cor and corplot

sat_tibble %>% select(-c(distance, weekend)) %>%
    cor() %>%
    corrplot::corrplot.mixed(upper = "ellipse")
# satisfaction items are moderately to strongly associated but not so much as to
# be identical (auto-correlated / collinear?) r > 0.8 for several or r > 0.9 for
# particular pairs

# Bivariate associations
sat_tibble %>%
    ggplot2::ggplot(aes(x = rides, y = overall)) +
    geom_point()

sat_tibble %>%
    ggplot2::ggplot(aes(x = clean, y = overall)) +
    geom_point()

# shows there is a tendency for people with higher satisfaction with rides and
# cleanlinesss to also have higher overall satisfaction



# fitting lm with 1 predictor  -----------------------------------

library(tidymodels)

# create a parsnip specification for a linear regression model
lm_spec <- linear_reg() %>%
    set_mode("regression") %>%
    set_engine("lm")
# it is slightly unnecessary to set the mode for linear regression but worth
# being explicit and consistent with use of parsnip for more complicated models

# we can now use this spec to fit some single predictor models
lm_spec %>%
    fit(overall ~ rides, data = sat_tibble)

# formula can be read as 'overall' varies with 'rides'
# when we call lm, r finds a line that best fits the relationship.
# R repeats the model and reports two coefficients
# - the intercept and the slope of the fitted line
# these can be used to provide the best estimate for any respondent's report of
# overall based on knowing his or her rides values.
# e.g. for customer who gives 90 rating for rides
# = intercept + slope * rides rating
-94.39 + 1.82 * 90
# = 69.41

lm_spec %>%
    fit(overall ~ clean, data = sat_tibble) %>%
    pluck("fit") %>%
    summary()

# we can save the model to use further
m1_rides <- lm_spec %>%
    fit(overall ~ rides, data = sat_tibble)

# we can go back to our scatterplot and overlay the fitted line
sat_tibble %>%
    ggplot2::ggplot(aes(x = rides, y = overall)) +
    geom_point() +
    geom_smooth(method='lm', formula= y~x)

# geom_smooth provides a confidence interval - can remove with se = FALSE
# (to match the books image)
sat_tibble %>%
    ggplot2::ggplot(aes(x = rides, y = overall)) +
    geom_point() +
    geom_smooth(method='lm', formula= y~x, se = FALSE)

# inspecting the model
str(m1_rides)
# in comparison to base lm (list of 12 items), tidymodel version has grouped into 5

# we can extract the coefficients this way
m1_rides$fit$coefficients
# rather than m1_rides$coefficients as would be used in base lm

# the fitted line we showed  earlier wasn't eplicitly using the model we created,
# so an alternative is to plug these coefficients from the model into an abline
sat_tibble %>%
    ggplot2::ggplot(aes(x = rides, y = overall)) +
    geom_point() +
    geom_abline(slope = 1.819959, intercept = -94.387908)
# this line is exactly the same as the one from geom_smooth

# view tidymodels simplified summary output
summary(m1_rides$fit)

m1_rides %>%
    pluck("fit") %>%
    summary()
# both do the same thing, in this case, for accessing single elements from a
# list the base r way does seem simpler

# more advanced models use the same summary output so worth being familiar

# can extract confidence intervals
m1_rides %>%
    pluck("fit") %>%
    confint()

# with simple linear regressions (one predictor)
# R^2 is equivalent to the correlation between the two variables - squared
cor(sat_tibble$overall, sat_tibble$rides) ^ 2


# checking fit lm 1 predictor ---------------------------------------------

# the ease of fitting linear models can lead to analysts fitting models and
# reporting results without considering whether the models are resonable

# Assumptions to be aware of:
# 1. Relationship between predictors and outcomes is linear (predictor can't be a square or above)
# - consequence if fitting y ~ x^2 - straight line will miss the curvature in
# relationship between x and y.
# 2. Prediction errors are normally distributed and look like random noise without pattern
# - examine plot of model's fitted values (predictions) vs. the residuals (prediction errors)

# toy model example
# generate data
toy_x_sq_tibble <- tibble(x = rnorm(500),
                          y = x ^ 2 + rnorm(500))
# fit model
toy_model <- lm_spec %>%
    fit(y ~ x, data = toy_x_sq_tibble)

toy_coef <- toy_model %>%
    pluck('fit') %>%
    pluck('coefficients')

# inspect bivariate scatter plot for assumption 1 - linear relationship
toy_x_sq_tibble %>%
    ggplot2::ggplot(aes(x, y)) +
    geom_point() +
    geom_abline(aes(slope = toy_coef[[1]],
                    intercept = toy_coef[[2]]))
# -- line does not capture relationship

# inspect fitted values vs. residuals for assumption 2 - normally distributed residuals
toy_x_sq_tibble %>%
    ggplot2::ggplot(aes(toy_model$fit$fitted.values, toy_model$fit$residuals)) +
    geom_point()
# -- clearly a pattern in residuals

# when face this problem with real data - solution is usually to transform x

# some common transformations:
# log(x), 1/x, 1/x^2, e^x/1+e^x, sqrt(x), x^2


# >> common transfomations for marketing data ----------------------------------------------------
# - unit sales, revenue, household income, price --> log(x)
# - distance --> 1/x, 1/x^2, log(x)
# - market or preference share based on utility value --> e^x/1+e^x
# - right-tailed distributions (generally) --> sqrt(x) or log(x) - watch out for log(x=<0)
# - left-tailed distributions (generally) --> x^2

### in most cases these are sufficient - however when these don't work
### or when want to find the best transformation - the box-cox transformation
### is a general purpose transformation function that can be used instead

# 4.5.5 - for more on Box-Cox transformations


# four plots to check fit 1 predictor -------------------------------------

library(cowplot)

# plot_grid(plot(m1_rides$fit))
# -- can come back to recreating this set of plots with tidymodels /
# tidyverse packages
par(mfrow = c(2,2))
plot(m1_rides$fit)
# top left - plot shows fitted values vs. residuals - no obvious pattern is good

# bottom left - fitted values vs. standardised residuals (sqrt)
# - pattern might indicate non-linear relationship
# - obs with high residuals flagged as outliers and r labels with row nums for inspection

# common pattern in residual plots - cone or funnel; range of errors increase for larger values
# -- indicates heteroskedasticity - values in one part of range have a larger spread
# + therefore undue influence on estimation of the line. a violation of linear model assumptions
# -- transformations can sometimes help

# top right - Normal Q-Q (Quantile-Quantile) plot - looking for points staying close to diagonal line
# - with some variation at ends expected - the line is where the residual values would be
# - expected to be if residuals follow a normal distribution

# bottom right - helps identify potential outliers - residuals vs. leverage
# - where leverage is the influence each point has on model coefficients.
# -- points with high residual (indicates different pattern) and high leverage (undue influence)
# - Cook's distance is an estimate of how much predicted values would change if
# -- model were re-estimated with that point eliminated. Model would show dotted lines
# -- for distance if have observations with high Cook's distance
# - observations labelled with row numbers to id potentially problematic outliers
# -- based on high standardised residual distance and leverage on the model.

# check identified outliers
sat_tibble %>% slice(67, 399, 457)
# only row 399 might be invalid with value for wait of 101

# check for other values with values over 100
sat_tibble %>% summary()
sat_tibble %>% filter(wait > 100 | clean > 100)
# four values identified

recode(sat_tibble$clean == 101, 100)

sat_tibble <- sat_tibble %>%
    mutate(clean = replace(clean, clean == 101, 100),
           wait = replace(wait, wait == 101, 100))

# rerun the model
m1_rides <- lm_spec %>%
    fit(overall ~ rides, data = sat_tibble)

m1_rides %>%
    pluck("fit") %>%
    summary()

m1_rides %>%
    pluck("fit") %>%
    confint()

par(mfrow = c(2,2))
plot(m1_rides$fit)
# still identifying the same values as potential outliers, so no massive impact
# of removing 101 values

# overall figures look good and suggests model relating overall satisfaction
# with rides is reasonable


# Fitting lm with multi predictors ----------------------------------------

# goal - sort through all features of the park to determine which ones
# are most closely related to overall satisfaction

m2_sat_vars <- lm_spec %>%
    fit(overall ~ rides + games + wait + clean, data = sat_tibble)

m2_sat_vars

m2_sat_vars %>%
    pluck("fit") %>%
    summary()

# evaluating model summary
# 1. R^2 imrpved to 0.5572 - just over half variation in overall ratings explained by
# ratings for specific features.
# 2, residual standard error is now 10.59 - predictions are more accurate (12.54 prev)
# 3. residuals also appear to be symmetric

par(mfrow = c(2,2))
plot(m2_sat_vars$fit)

# 4. examine model coefficients
# - each coef represents strength of relationship btw satisfaction with that
# feature and overall satisfaction, conditional on values of other predictors
# -  all 4 identified with statistically significant p-value
# - helpful to visualise the coeficients


coefplot::coefplot(m2_sat_vars$fit)
# need to remove intercept to make out just the coefficients
coefplot::coefplot(m2_sat_vars$fit, intercept = FALSE)
m2_sat_vars %>% pluck('fit') %>% pluck('coefficients')


# coef plot  --------------------------------------------------------------
# often a key output of satisfaction drivers analysis

# args - set confint to 1.96 se = 95% CI + increase size of plotted lines
coefplot::coefplot(m2_sat_vars$fit, intercept = FALSE,
                   outerCI = 1.96, lwdOuter = 1.5,
                   ylab = "Rating of Feature",
                   xlab = "Association with Overall Satisfaction")

# cleanliness estimated to be most important feature associated with overall
# satisfaction, followed by satisfaction with rides and then wait times.
# Satisfaction with games estimated to be relatively less important.

# sorting so coefficients in order based on estimated coefficient makes it
# easier to quickly id important features




